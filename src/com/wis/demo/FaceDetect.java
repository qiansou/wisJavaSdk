package com.wis.demo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.wis.constant.Config;
import com.wis.util.image.ParserImage;
import com.wis.util.image.RotateImage;
import com.wis.util.rpc.HttpClient;

public class FaceDetect {

	/**
	 * @author vincent ln1394@163.com
     * @date 2017-9-24
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String picUrl = "d:/1.jpg";
		System.out.println(detect(picUrl));
	}
	/*
	 * @param picUrl
	 */
	public static String detect(String picUrl){
		String img = "" ;
		File pic = new File(picUrl);
		//校验图片是否是正位，非正位进行校正
		int rs = RotateImage.rotate(pic);
		if(rs != 0){//校正成功，如果非正位  影响识别结果
			System.out.println("rotate image failed!");
		}
		//调用开放平台人脸识别服务
		//生成base64参数
		String p1 = ParserImage.getImageStr(picUrl);	    	
		Map map = new HashMap();
		map.put("faceimage", p1);
		img = HttpClient.sendHttpPostJson(Config.facedetect,JSONObject.fromObject(map).toString());
		System.out.println(img);
		return img;
	}

}
