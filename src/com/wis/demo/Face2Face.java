package com.wis.demo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.wis.constant.Config;
import com.wis.util.image.ParserImage;
import com.wis.util.image.RotateImage;
import com.wis.util.rpc.HttpClient;

public class Face2Face {

	/**
	 * @author vincent ln1394@163.com
     * @date 2017-9-24
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String picUrl1 = "d:/1.jpg";
		File pic = new File(picUrl1);
		//校验图片是否是正位，非正位进行校正
		int rs = RotateImage.rotate(pic);
		if(rs != 0){//校正成功，如果非正位  影响识别结果
			System.out.println("rotate image failed!");
		}
		//调用开放平台人脸识别服务
		//生成base64参数
		String faceimage1 = ParserImage.getImageStr(picUrl1);
		//服务器压力顶不住
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String picUrl2 = "d:/0.jpg";
		pic = new File(picUrl2);
		//校验图片是否是正位，非正位进行校正
		rs = RotateImage.rotate(pic);
		if(rs != 0){//校正成功，如果非正位  影响识别结果
			System.out.println("rotate image failed!");
		}
		//调用开放平台人脸识别服务
		//生成base64参数
		String faceimage2 = ParserImage.getImageStr(picUrl1);
		
		//服务器压力顶不住
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(compare(faceimage1,faceimage2)); 
	}
	/*
	 * @param picUrl
	 */
	public static String compare(String feature1,String feature2){
		String img = "" ;
		//调用开放平台人脸识别服务	    	
		Map map = new HashMap();
		map.put("faceimage1", feature1);
		map.put("faceimage2", feature2);
		img = HttpClient.sendHttpPostJson(Config.face2face,JSONObject.fromObject(map).toString());
		return img;
	}

}
